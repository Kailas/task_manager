package fr.um.projet.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

public class Resources {
	
	public static final String path = "res/";
	
	private static String ENCODAGE = "UTF8";
	
	private static HashMap<String, String> strings = new HashMap<String, String>();
	private static HashMap<String, String> configs = new HashMap<String, String>();
	private static  ArrayList<String> categories = new ArrayList<String>();
	
	static { 
		read(path + "config", configs);
		read(path + "strings_" + getConfig("lang"), strings);
		read(path + "categories", categories);
		Collections.sort(categories);
	};
	
	private static void read(String file, HashMap<String, String> values) {
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), ENCODAGE));
			String line;
			while ((line = br.readLine()) != null){
				line = line.replaceAll(": ", ":");
				int k = line.indexOf(':');
				values.put(line.substring(0, k), line.substring(k + 1));
			}
			br.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
	}
	
	private static void read(String file, ArrayList<String> categories) {
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), ENCODAGE));
			String line;
			while ((line = br.readLine()) != null){
				categories.add(line);
			}
			br.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private static String get(String name, HashMap<String, String> values) {
		String res = values.get(name);
		return res == null ? "null" : res;
	}
	
	public static ArrayList<String> readCategory() {
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(path + "categories"), ENCODAGE));
			String line;
			while ((line = br.readLine()) != null){
				list.add(line);
			}
			br.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Collections.sort(list);
		return list;
	}
	
	public static void writeCategory(String category) {
		File f = new File(path + "categories");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true), ENCODAGE));
			bw.write(category + "\n");
			bw.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void writeCategories(ArrayList<String> categories) {
		File f = new File(path + "categories");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, false), ENCODAGE));
			for (String cat : categories)
				bw.write(cat + "\n");
			bw.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * Retourne la valeur associ&eacute;e a la cl&eacute; name. Retourne "null" sinon. 
	 * @param name Nom de la cha&icirc;ne de caract&egrave;res &agrave; r&eacute;cup&eacute;rer.
	 * @return La chaine de caract&egrave;res associ&eacute;e au nom.
	 */
	public static String getString(String name) {
		return get(name, strings);
	}
	
	public static String getConfig(String name) {
		return get(name, configs);
	}
	
	public static ArrayList<String> getCategories() {
		return categories;
	}

	public static HashMap<String, String> getConfig() {
		return configs;
	}
	
	public static void writeConfig(HashMap<String, String> map)  {
		File f = new File(path + "config");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, false), ENCODAGE));
			for (Entry<String, String> entry : map.entrySet()) {
				bw.write(entry.getKey() + ": " + entry.getValue() + "\n");
			}
				
			bw.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
