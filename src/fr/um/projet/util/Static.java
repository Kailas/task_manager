package fr.um.projet.util;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import fr.um.projet.Task;
import fr.um.projet.controller.ApplicationController;

public class Static {

	private static ArrayList<Task> tasks = null;
	private static HashMap<String, ImageIcon> icons;
	private static ApplicationController controller;
	// Enregistre la tache a modifier
	private static Task task;	
	private static Locale locale;
	
	public static ArrayList<Task> getTasks() {
		if (tasks == null)
			tasks = Task.getTasks();
		return tasks;
	}
	
	public static Calendar stringToDate(String date) {
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return calendar;
	}
	
	public static String calendarToString(Calendar c) {
		return (new SimpleDateFormat("dd/MM/yyyy")).format(c.getTime());
	}
	
	public static String getToday() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(new Date());
	}
	
	public static int compareDateWithToday(String date) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return compareDate(date, format.format(new Date()));
	}
	
	/**
	 * Retourne le nombres de jours entre les deux dates. Si date2 sup&eacute;rieur &agrave; date1, le r&eacute;sultat sera sup&eacute;rieur &acirc; 
	 * 0. Sinon inf&eacute;rieur ou &eacute;gal &acirc; 0.
	 * @param date1 La premi&egrave;re date.
	 * @param date2 La seconde date.
	 * @return Le nombre de jours s&eacute;rant les deux dates.
	 */
	public static int compareDate(String date1, String date2) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(date1);
			d2 = format.parse(date2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return (int) ((d2.getTime() - d1.getTime()) / (24 * 60 * 60 * 1000));
	}
	
	/**
	 * Retourne l'icone associ&eacute; au nom. Si l'icone n'existe pas, il sera cr&eacute;&eacute;
	 * @param name Le nom de l'icone. (Doit se trouver dans le dossier res/icon/ et avoir l'extension jpg ou png.
	 * @return L'icone associ&eacute; au nom.
	 */
	public static ImageIcon getIcon(String name) {
		if (icons == null) {
			icons = new HashMap<String, ImageIcon>();
		}
		ImageIcon icon = null;
		if ((icon = icons.get(name)) == null) {
			try {
				icons.put(name, icon = new ImageIcon(ImageIO.read(new File("res/icon/" + name + ".png"))));
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return icon;
	}
	
	public static void setApplicationController(ApplicationController controller) {
		Static.controller = controller;
	}
	
	public static void changePage(String name) {
		controller.changePage(name);
	}

	public static void setTask(Task task) {
		Static.task = task;
	}
	
	public static Task getTask() {
		return task;
	}
	
	/**
	 * Retourne la couleur &acirc; partir d'un String.
	 * @param c Le String &acirc; convertir en Color.
	 * @return La couleur.
	 */
	public static Color toColor(String c) {
		int r = Integer.valueOf(c.substring(0, 3));
		int g = Integer.valueOf(c.substring(3, 6));
		int b = Integer.valueOf(c.substring(6, 9));
		return new Color(r, g, b);
	}
	
	public static String rgbToStringColor(final String r, final String g, final String b) {
		String rf = r;
		String gf = g;
		String bf = b;
		if (rf.length() == 1) {
			rf = "00" + rf;
		} else if (rf.length() == 2) {
			rf = "0" + rf;
		}
		if (gf.length() == 1) {
			gf = "00" + gf;
		} else if (gf.length() == 2) {
			gf = "0" + gf;
		}
		if (bf.length() == 1) {
			bf = "00" + bf;
		} else if (bf.length() == 2) {
			bf = "0" + bf;
		}
		return rf + gf + bf;
	}
	
	public static Locale getLocale() {
		if (Resources.getConfig("lang").equals("fr"))
			locale = Locale.FRENCH;
		else if (Resources.getConfig("lang").equals("en"))
			locale = Locale.ENGLISH;
		return locale;
	}
	
 }
