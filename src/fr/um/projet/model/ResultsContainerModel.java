package fr.um.projet.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import fr.um.projet.Task;
import fr.um.projet.util.Static;

/**
 * Le model rattach&eacute; au model MVC de la classe bilan.
 * 
 */
public class ResultsContainerModel extends ContainerModel {
	
	private ArrayList<Task> tasks;
	private ArrayList<Task> tasksInProgress;
	private ArrayList<Task> tasksInLate;
	private ArrayList<Task> tasksFinished;
	
	/**
	 * Cr&eacute;e un BilanContainerModel vide.
	 */
	public ResultsContainerModel() {
		tasksInProgress = new ArrayList<Task>();
		tasksInLate = new ArrayList<Task>();
		tasksFinished = new ArrayList<Task>();
	}
	
	/**
	 * Retourne la liste de t&acirc;ches.
	 * @return La liste de t&acirc;ches.
	 */
	public final ArrayList<Task> getTasks() {
		return tasks;
	}
	
	/**
	 * Retourne la liste des t&acirc;ches en cours.
	 * @return La liste des t&acirc;ches en cours.
	 */
	public final ArrayList<Task> getTasksInProgress() {
		return tasksInProgress;
	}
	
	/**
	 * Retourne la liste des t&acirc;ches en retard.
	 * @return La liste des t&acirc;ches en retard.
	 */
	public final ArrayList<Task> getTasksInLate() {
		return tasksInLate;
	}
	
	/**
	 * Retourne la liste des t&acirc;ches termin&eacute;es.
	 * @return La liste des t&acirc;ches termin&eacute;es.
	 */
	public final ArrayList<Task> getTasksFinished() {
		return tasksFinished;
	}
	
	/**
	 * Sp&eacute;cifie la date de d&eacute;but et de fin des t&acirc;ches &agrave; afficher.
	 * La liste de t&acirc; est actualis&eacute;e en fonction de ses dates.
	 * @param date1 La date de d&eacute;but.
	 * @param date2 La date de fin.
	 */
	public void setDate(final String date1, final String date2) {
		tasksInProgress.clear();;
		tasksInLate.clear();;
		tasksFinished.clear();
		tasks = Task.getTasks();
		Collections.sort(tasks, new Comparator<Task>() {

			@Override
			public int compare(Task t1, Task t2) {
				return -Static.compareDate(t1.getDate(), t2.getDate());
			}
			
		});
		for (int k = 0; k < tasks.size(); ++k) {
			Task t = tasks.get(k);
			if (!(Static.compareDate(date1, t.getDate()) >=0 && Static.compareDate(t.getDate(), date2) >= 0)) {
				tasks.remove(k);
				--k;
			} else {
				if (t.isFinished()) {
					tasksFinished.add(t);
				} else if (!t.isLate()) {
					tasksInProgress.add(t);
				} else {
					tasksInLate.add(t);
				}
			}
		}
	}
	
	/**
	 * Retourne la liste de t&acirc;ches des modifications d'une t&acirc;che depuis sa cr&eacute;ation.
	 * @param id L'identifiant de la t&acirc;che.
	 * @return La liste de t&acirc;ches des modifications d'une t&acirc;che depuis sa cr&eacute;ation.
	 */
	public final ArrayList<Task> getTasksModify(final int id) {
		Task task = null;
		for (Task t : Task.getTasks()) {
			if (t.getId() == id) {
				task = t;
				break;
			}
		}
		ArrayList<Task> tasks = new ArrayList<Task>();
		while ((task = task.getPreviousTask()) != null) {
			tasks.add(task);
		}
		return tasks;
	}
	
}
