package fr.um.projet.model;

import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import fr.um.projet.Task;
import fr.um.projet.controller.ContainerController;

/**
 * Le model rattach&eacute; au model MVC de la classe application.
 * G&egrave;re la partie m&eacute;tier de l'application.
 */
public class ApplicationModel {

	private ArrayList<Task> taskInProgress;
	private ArrayList<Task> taskFinished;
	private ArrayList<Task> taskInLate;
	private ContainerController containerController;
	
	/**
	 * Comparaison entre deux dates.
	 * @param d1 La premi&egrave;re date.
	 * @param d2 La seconde date.
	 * @return -1 si d1 inf&eacute;rieure d2, 0 si d1 &eacute;gale d2, 1 sinon.
	 */
	public final int compareDate(final String d1, final String d2) {
		int day1 = Integer.parseInt(d1.substring(0, 2));
		int m1 = Integer.parseInt(d1.substring(3, 5));
		int y1 = Integer.parseInt(d1.substring(6, 10));
		int day2 = Integer.parseInt(d2.substring(0, 2));
		int m2 = Integer.parseInt(d2.substring(3, 5));
		int y2 = Integer.parseInt(d2.substring(6, 10));
		if (y1 > y2) {
			return 1;
		}
		if (y1 < y2) {
			return -1;
		}
		if (m1 < m2) {
			return -1;
		}
		if (m1 > m2) {
			return 1;
		}
		if (day1 < day2) {
			return -1;
		}
		if (day1 > day2) {
			return 1;
		}
		return 0;
	}
	
	/**
	 * Trie une liste de t&acirc;ches par rapport &acirc; leur date.
	 * @param tasks La liste de t&acirc;ches.
	 */
	public void sortSimple(ArrayList<Task> tasks) {
		Collections.sort(tasks, new Comparator<Task>() {

			@Override
			public int compare(Task t1, Task t2) {
				return compareDate(t1.getDate(), t2.getDate());
			}
			
		});
	}
	
	/**
	 * Trie une liste de t&acirc;ches en trois listes de t&acirc;ches en fonction de leur &eacute;tat (En cours, termin&eacute;e ou en retard).
	 * @param tasks La liste de t&acirc;ches.
	 */
	public void sortTaskByState(final ArrayList<Task> tasks) {
		taskInProgress = new ArrayList<Task>();
		taskInLate = new ArrayList<Task>();
		taskFinished = new ArrayList<Task>();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		for (Task t : tasks) {
			if (t.isFinished()) {
				taskFinished.add(t);
			} else if (compareDate(format.format(date), t.getDate()) <= 0) {
				taskInProgress.add(t);
			} else {
				taskInLate.add(t);
			}
		}
	}

	/**
	 * Retourne la liste des t&acirc;ches termin&eacute;es.
	 * @return La liste des t&acirc;ches termin&eacute;es.
	 */
	public final ArrayList<Task> getTaskInProgress() {
		return taskInProgress;
	}

	/**
	 * Retourne la liste des t&acirc;ches termin&eacute;es.
	 * @return La liste des t&acirc;ches termin&eacute;es.
	 */
	public final ArrayList<Task> getTaskFinished() {
		return taskFinished;
	}
	
	/**
	 * Retourne la liste des t&acirc;ches en retard.
	 * @return La liste des t&acirc;ches en retard.
	 */
	public final ArrayList<Task> getTaskInLate() {
		return taskInLate;
	}

	/**
	 * Retourne le comonent view associéeacute; au mod&egrave;le MVC.
	 * @return Le component view associ&eacute; au mod&egrave;le MVC.
	 */
	public final Component getContainer() {
		return this.containerController.getView();
	}

	/**
	 * Sp&eacute;cifie le controller associ&eacute; au mod&egrave;le MVC.
	 * @param containerController Le controller.
	 */
	public void setContainer(final ContainerController containerController) {
		this.containerController = containerController;
	}
	
}
