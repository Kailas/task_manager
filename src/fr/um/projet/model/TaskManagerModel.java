package fr.um.projet.model;

import java.util.ArrayList;

import fr.um.projet.Task;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

/**
 * Le model rattach&eacute; au model MVC de la classe TaskManager.
 * G&egrave;re la partie m&eacute;tier de la partie de cr&eacute;ation/modification des t&acirc;ches.
 */
public class TaskManagerModel extends ContainerModel {
	
	private ArrayList<String> categories = Resources.readCategory();
	private String catSelected = null;
	private Task task;
	
	/**
	 * Cr&eacute;e un TaskManagerModel vide.
	 */
	public TaskManagerModel() {
		if (Static.getTask() == null) {
			task = new Task();
			task.setDate(Static.getToday());
			task.setCreationDate(Static.getToday());
		} else {
			task = Static.getTask();
			Static.setTask(null);
		}
	}
	
	/**
	 * Cr&eacute;e une nouvelle t&acirc;che.
	 * @param name Le nom de la t&acirc;che.
	 * @param type Le type de la t&acirc;che.
	 * @param category La cat&eacute;gorie de la t&acirc;che.
	 * @param dateBegin La date de d&eacute;but de la t&acirc;che.
	 * @param dateEnd La date de fin de la t&acirc;che.
	 * @param percent Le pourcentage de progresstion de la t&acirc;che.
	 */
	public void createTask(final String name, final String type, final String category, final String dateBegin, final String dateEnd, final int percent) {
		task.setName(name);
		task.setType(type);
		task.setCategory(category);
		task.setCreationDate(dateBegin);
		task.setDate(dateEnd);
		task.setProgress(percent);
		task.write();
	}
	
	/**
	 * Ajoute une cat&eacute;gorie &agrave; la liste des cat&eacute;gories.
	 * @param category Le nom de la nouvelle cat&eacute;gorie.
	 */
	public void addCategory(final String category) {
		if (categories.contains(category)) {
			return;
		}
		Resources.writeCategory(category);
		categories = Resources.readCategory();
		catSelected = category;
	}
	
	/**
	 * Retourne la liste des cat&eacute;gories.
	 * @return La liste des cat&eacute;gories.
	 */
	public final ArrayList<String> getCategories() {
		return categories;
	}
	
	/**
	 * Retourne le nom de la cat&eacute;gorie s&eacute;lectionn&eacute;e.
	 * @return Le nom de la cat&eacute;gorie s&eacute;lectionn&eacute;e.
	 */
	public final String getCategorySelected () {
		return catSelected;
	}
	
	/**
	 * Retourne le nom de la t&acirc;che.
	 * @return Le nom de la t&acirc;che.
	 */
	public final String getName() {
		return task.getName();
	}
	
	/**
	 * Retourne le type de la t&acirc;che.
	 * @return Le type de la t&acirc;che.
	 */
	public final String getType() {
		return task.getType();
	}
	
	/**
	 * Retourne la cat&eacute;gorie de la t&acirc;che.
	 * @return La cat&eacute;gorie de la t&acirc;che.
	 */
	public final String getCategory() {
		return task.getCategory();
	}
	
	/**
	 * Retourne la date de d&eacute;but.
	 * @return La date de d&eacute;but.
	 */
	public final String getDateBegin() {
		return task.getCreationDate();
	}
	
	/**
	 * Retourne la date de fin.
	 * @return La date de fin.
	 */
	public final String getDateEnd() {
		return task.getDate();
	}
	
	/**
	 * Retourne le pourcentage de progression
	 * @return Le pourcentage de progression
	 */
	public final int getPercent() {
		return task.getProgress();
	}
}
