package fr.um.projet.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import fr.um.projet.Task;
import fr.um.projet.util.Static;

/**
 * Le model rattach&eacute; au model MVC de la classe TaskContainerMenu.
 * G&egrave;re la partie m&eacute;tier de la partie TaskContainerMenu.
 */
public class TasksContainerMenuModel extends ContainerMenuModel {
	
	private String type;
	private boolean taskEditable;
	private boolean taskValidable;
	private boolean taskRemovable;
	
	/**
	 * Cr&eacute;e un TaskContainerMenuModel vide.
	 * @param type Le type des t&acirc;ches contenu dans cette classe. (Type : fini ou non).
	 */
	public TasksContainerMenuModel(final String type) {
		this.type = type;
		if (type.equals("finish")) {
			taskEditable = false;
			taskValidable = false;
			taskRemovable = true;
		} else {
			taskEditable = true;
			taskValidable = true;
			taskRemovable = true;
		}
	}
	
	/**
	 * Retourne la liste des t&acirc;ches en fonction du type du Component.
	 * @return La liste des t&acirc;ches en fonction du type du Component
	 */
	public final ArrayList<Task> getTasks() {
		ArrayList<Task> tasks = Task.getTasks();
		for (int k = 0; k < tasks.size(); ++k) {
			if (type.equals("all")) {
				if (tasks.get(k).isFinished()) {
					tasks.remove(k);
				}
			} else if (!((type.equals("progress") && !tasks.get(k).isLate() && !tasks.get(k).isFinished()) 
					|| (type.equals("finish") && tasks.get(k).isFinished())
					|| (type.equals("late") && !tasks.get(k).isFinished() && tasks.get(k).isLate()))) {
				tasks.remove(k);
				--k;
			}
		}
		Collections.sort(tasks, new Comparator<Task>() {

			@Override
			public int compare(Task t1, Task t2) {
				int date = -Static.compareDate(t1.getDate(), t2.getDate());
				if (date == 0) {
					if (t1.isFinished() && t2.isFinished()) {
						return 0;
					}
					if (t1.isFinished() && !t2.isFinished()) {
						return 1;
					}
					if (!t1.isFinished() && t2.isFinished()) {
						return -1;
					}
					if (t1.isLate() && !t2.isLate()) {
						return -1;
					}
					if (!t1.isLate() && t2.isLate()) {
						return 1;
					}
					return 0;
				}
				return date;
			}
			
		});
		return tasks;
	}
	
	/**
	 * Change le type des t&acirc;ches contenues dans ce Component.
	 * @param type Le type des t&acirc;ches contenues dans ce Component.
	 */
	public void changeTasksPanel(final String type) {
		this.type = type;
		if (type.equals("finish")) {
			taskEditable = false;
			taskValidable = false;
			taskRemovable = true;
		} else {
			taskEditable = true;
			taskValidable = true;
			taskRemovable = true;
		}
	}
	
	/**
	 * Met la t&acirc;che &agrave; l'&eacute;tat fini et la sauvegarde.
	 * @param t La t&acirc;che &agrave; mettre &agrave; l'&eacute;tat fini.
	 */
	public void finishTask(final Task t) {
		if (t.getType().equals("long")) {
			t.setProgress(100);
		}
		t.setFinished(true);
		t.write();
	}
	
	/**
	 * Supprime la t&acirc;che.
	 * @param t La t&acirc;che &agrave; supprimer.
	 */
	public void removeTask(final Task t) {
		t.remove();
	}
	
	/**
	 * Retourne true ou false suivant que la t&acirc;che est modifiable ou pas.
	 * @return true ou false suivant que la t&acirc;che est modifiable ou pas.
	 */
	public final boolean taskEditable() {
		return taskEditable;
	}

	/**
	 * Retourne le type de t&acirc;ches.
	 * @return Le type de t&acirc;ches.
	 */
	public String getType() {
		return type;
	}

	public boolean taskValidable() {
		return taskValidable;
	}

	public boolean taskRemovable() {
		return taskRemovable;
	}
	
}
