package fr.um.projet.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import fr.um.projet.Task;
import fr.um.projet.util.Resources;

/**
 * Le model rattach&eacute; au model MVC de la classe SettingsContainerMenuModel.
 * G&egrave;re la partie m&eacute;tier de la partie settings.
 */
public class SettingsContainerMenuModel extends ContainerMenuModel {
	
	private String type;
	private ArrayList<String> cat;
	
	/**
	 * Cr&eacute;e un SettingsContainerMenuModel vide.
	 */
	public SettingsContainerMenuModel() {
		cat = Resources.readCategory();
	}
	
	/**
	 * Sp&eacute;cifie la nouvelle page &agrave; afficher.
	 * @param type Le nom de la page.
	 */
	public void changePage(final String type) {
		this.type = type;
	}
	
	/**
	 * Retourne la liste des cat&eacute;gories.
	 * @return La liste des cat&eacute;gories.
	 */
	public final ArrayList<String> getCategories() {
		return cat;
	}
	
	/**
	 * Supprime une cat&eacute;gorie de la liste des cat&eacute;gories.
	 * @param index Indice de la cat&eacute; &acirc; supprimer.
	 */
	public void deleteCategory(final int index) {
		ArrayList<Task> tasks = Task.getTasks();
		for (Task t : tasks) {
			if (t.getCategory().equals(cat.get(index))) {
				t.setCategory("");
				t.write();
			}
		}
		cat.remove(index);
	}
	
	/**
	 * Retourne le nom de la page.
	 * @return Le nom de la page.
	 */
	public final String getPage() {
		return type;
	}
	
	/**
	 * Recharge la liste des cat&eacute;gories.
	 */
	public void resetCategory() {
		cat = Resources.readCategory();
	}

	/**
	 * Sauvegarde la liste de cat&eacute;gories.
	 * @param arrayList La liste des cat&eacute;gories.
	 */
	public void saveCategory(final ArrayList<String> arrayList) {
		ArrayList<Task> tasks = Task.getTasks();
		for (Task t : tasks) {
			for (int k = 0; k < cat.size(); ++k) {
				if ((arrayList.get(k) == null || !cat.get(k).equals(arrayList.get(k))) && t.getCategory().equals(cat.get(k))) {
					if (arrayList.get(k) == null) {
						t.setCategory("");
					} else {
						t.setCategory(arrayList.get(k));
					}
					t.write();
				}
			}
		}
		cat.clear();
		for (int k = 0; k < arrayList.size(); ++k) {
			if (arrayList.get(k) != null)
				cat.add(arrayList.get(k));
		}
		Collections.sort(cat);
		Resources.writeCategories(cat);
		cat = Resources.getCategories();
	}
	
	/**
	 * Modifie le nom d'une cat&eacute;gorie.
	 * @param text Le nouveau nom de la cat&eacute;gorie.
	 * @param index L'indice de la cat&eacute;gorie.
	 */
	public void changeCategory(final String text, final int index) {
		System.out.println("changeCatergory");
		ArrayList<Task> tasks = Task.getTasks();
		for (Task t : tasks) {
			System.out.println(t.getCategory() + "    " +  cat.get(index));
			if (t.getCategory().equals(cat.get(index))) {
				t.setCategory(text);
				t.write();
				System.out.println("Tache ecrite");
			}
		}
		cat.remove(index);
		cat.add(text);
		Collections.sort(cat);
	}

	/**
	 * Retourne la Hashmap contenant les configs.
	 * @return La Hashmap contenant les configs
	 */
	public final HashMap<String, String> getGeneralSettings() {
		return Resources.getConfig();
	}
	
	
	public void updateGeneralSettings(HashMap<String, String> map) {
		Resources.writeConfig(map);
	}

	public void addCategory(String text) {
		cat.add(text);
	}
}
