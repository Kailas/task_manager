package fr.um.projet;

import javax.swing.SwingUtilities;

import fr.um.projet.controller.ApplicationController;

public class Application {

	public Application() {
		new ApplicationController();
	}
	
	public static final void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new Application();
			}
		});
	}
	
}
