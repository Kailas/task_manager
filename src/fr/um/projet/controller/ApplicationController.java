package fr.um.projet.controller;

import fr.um.projet.model.ApplicationModel;
import fr.um.projet.util.Static;
import fr.um.projet.view.ApplicationView;

/**
 * Le controller rattach&eacute; au model MVC de la classe application.
 */
public class ApplicationController {
	
	private ApplicationView view;
	private ApplicationModel model;
	
	/**
	 * Cr&eacute;e un ApplicationController vide.
	 */
	public ApplicationController() {
		this.model = new ApplicationModel();
		this.model.setContainer(new TasksContainerMenuController());
		this.view = new ApplicationView(this, model);
		Static.setApplicationController(this);
	}
	
	/**
	 * Action &agrave; effectuer lors d'un clique sur un bouton du menu.
	 * @param name Nom du bouton.
	 */
	public void menuAction(String name) {
		this.changePage(name);
	}
	
	/**
	 * Change la page.
	 * @param name Nom de la nouvelle page.
	 */
	public void changePage(String name) {
		if (name.equals("tasks")) {
			this.model.setContainer(new TasksContainerMenuController());
			this.view.updateButton();
			this.view.update();
		} else if (name.equals("newTask")) {
			this.model.setContainer(new TaskManagerController());
			this.view.update();
		} else if (name.equals("bilan")) {
			this.model.setContainer(new ResultsContainerController());
			this.view.update();			
		} else if (name.equals("settings")) {
			this.model.setContainer(new SettingsContainerMenuController());
			this.view.update();			
		} else {
			throw new AssertionError("Nom du bouton inconnu");
		}
	}
		
}
