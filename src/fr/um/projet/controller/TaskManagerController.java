package fr.um.projet.controller;

import fr.um.projet.model.TaskManagerModel;
import fr.um.projet.util.Static;
import fr.um.projet.view.TaskManagerView;

/**
 * Le controller rattach&eacute; au model MVC de la classe TaskManager.
 */
public class TaskManagerController extends ContainerController {
	
	/**
	 * Cr&eacute;e un TaskManagerController vide.
	 */
	public TaskManagerController() {
		super();
		this.model = new TaskManagerModel();
		this.view = new TaskManagerView(this, (TaskManagerModel) this.model);
	}
	
	/**
	 * Ajoute une nouvelle t&acirc;che.
	 * @param name Le nom de la t&acirc;che.
	 * @param type Le type de la t&acirc;che.
	 * @param category La cat&eacute;gorie de la t&acirc;che.
	 * @param dateBegin La date de d&eacute;but de la t&acirc;che.
	 * @param dateEnd La date de fin de la t&acirc;che.
	 * @param percent Le pourcentage de progression de la t&acirc;che.
	 */
	public void addTask(String name, String type, String category, String dateBegin, String dateEnd, int percent) {
		((TaskManagerModel) this.model).createTask(name, type, category, dateBegin, dateEnd, percent);
		Static.changePage("tasks");
	}
	
	/**
	 * Ajoute une cat&eacute;gorie.
	 * @param category Nom de la cat&eacute;gorie.
	 */
	public void addCategory(String category) {
		((TaskManagerModel) this.model).addCategory(category);
	}
}
