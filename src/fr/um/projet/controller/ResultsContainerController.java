package fr.um.projet.controller;

import fr.um.projet.model.ResultsContainerModel;
import fr.um.projet.view.ResultsContainerView;

/**
 * Le controller rattach&eacute; au model MVC de la classe bilan.
 * 
 */
public class ResultsContainerController extends ContainerController {
	
	/**
	 * Cr&eacute;e un BilanContainerController vide.
	 */
	public ResultsContainerController() {
		super();
		this.model = new ResultsContainerModel();
		this.view = new ResultsContainerView(this, (ResultsContainerModel) this.model);
	}
	
	/**
	 * Sp&eacute;cifie la date de d&eacute;but et de fin du bilan.
	 * @param dateMin Date de d&eacute;but.
	 * @param dateMax Date de fin.
	 */
	public void setDate(String dateMin, String dateMax) {
		((ResultsContainerModel) this.model).setDate(dateMin, dateMax);
	}
	
}
