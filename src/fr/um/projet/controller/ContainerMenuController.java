package fr.um.projet.controller;

import fr.um.projet.model.ContainerMenuModel;
import fr.um.projet.view.ContainerMenuView;

/**
 * Le model rattach&eacute; au model MVC de la classe ContainerMenu.
 *
 */
public class ContainerMenuController extends ContainerController {
	
	/**
	 * Cr&eacute;e un ContainerMenuController vide.
	 */
	public ContainerMenuController() {
		super();
		model = new ContainerMenuModel();
		view = new ContainerMenuView(this, (ContainerMenuModel) this.model);
	}
	
}
