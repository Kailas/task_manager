package fr.um.projet.controller;

import java.awt.Component;

import fr.um.projet.model.ContainerModel;
import fr.um.projet.view.ContainerView;

/**
 * Le controller rattach&eacute; au model MVC de la classe Container.
 *
 */
public class ContainerController {
	
	protected ContainerView view;
	protected ContainerModel model;
	
	/**
	 * Cr&eacute;e un ContainerModel vide.
	 */
	public ContainerController() {
		model = new ContainerModel();
		view = new ContainerView(this, this.model);
	}
	
	/**
	 * Cr&eacute;e un ContainerModel vide.
	 * @param view Vue attach&eacute; &agrave; ce controller.
	 * @param model Model attach&eacute; &agrave; ce controller.
	 */
	public ContainerController(ContainerView view, ContainerModel model) {
		this.model = model;
		this.view = view;
	}
	
	/**
	 * Retourne la vue attach&eacute; &agrave; ce controller.
	 * @return La vue attach&eacute; &agrave; ce controller.
	 */
	public Component getView() {
		return this.view;
	}
}
