package fr.um.projet.controller;

import fr.um.projet.Task;
import fr.um.projet.model.TasksContainerMenuModel;
import fr.um.projet.view.TasksContainerMenuView;

/**
 * Le controller rattach&eacute; au model MVC de la classe TaskContainerMenu.
 */
public class TasksContainerMenuController extends ContainerMenuController {
	
	/**
	 * Cr&eacute;e un TaskContainerMenuController vide.
	 */
	public TasksContainerMenuController() {
		super();
		this.model = new TasksContainerMenuModel("all");
		this.view = new TasksContainerMenuView(this, (TasksContainerMenuModel) this.model);
	}

	/**
	 * Associe un bouton &agrave; une action.
	 * @param type Le type du bouton.
	 */
	public void buttonAction(String type) {
		((TasksContainerMenuModel) this.model).changeTasksPanel(type);
		((TasksContainerMenuView) this.view).updateTasks();
	}

	/**
	 * Met l'&eacute;tat de la t&acirc;che &agrave; fini.
	 * @param t La t&acirc;che &agrave; mettre l'&eacute;tat &agrave; fini.
	 */
	public void finishTask(Task t) {
		((TasksContainerMenuModel) this.model).finishTask(t);
		((TasksContainerMenuView) this.view).updateTasks();
	}
	
	/**
	 * Supprime une t&acirc;che.
	 * @param t La t&acirc;che &agrave; supprimer.
	 */
	public void removeTask(Task t) {
		((TasksContainerMenuModel) this.model).removeTask(t);
		((TasksContainerMenuView) this.view).updateTasks();
	}
	
	
}
