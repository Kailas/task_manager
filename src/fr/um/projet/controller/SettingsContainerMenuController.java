package fr.um.projet.controller;

import java.util.ArrayList;
import java.util.HashMap;

import fr.um.projet.model.ContainerMenuModel;
import fr.um.projet.model.SettingsContainerMenuModel;
import fr.um.projet.view.SettingsContainerMenuView;

/**
 * Le controller rattach&eacute; au model MVC de la classe SettingsContainerMenuModel.
 */
public class SettingsContainerMenuController extends ContainerMenuController {
	
	/**
	 * Cr&eacute;e une SettingsContainerMenuController vide.
	 */
	public SettingsContainerMenuController() {
		super();
		this.model = new SettingsContainerMenuModel();
		this.view = new SettingsContainerMenuView(this, (ContainerMenuModel) this.model);
		this.buttonAction("category");
	}
	
	/**
	 * Associe une action au bouton.
	 * @param type Le type du bouton.
	 */
	public void buttonAction(String type) {
		((SettingsContainerMenuModel) this.model).changePage(type);
		((SettingsContainerMenuModel) this.model).resetCategory();
		((SettingsContainerMenuView) this.view).update();
	}
	
	/**
	 * Supprime une cat&eacute;gorie.
	 * @param index Indice de la cat&eacute;gorie.
	 */
	public void deleteCategory(int index) {
		((SettingsContainerMenuModel) this.model).deleteCategory(index);
	}
	
	/**
	 * Remet &agrave; z&eacute;ro la page.
	 * @param page La page &agrave; remettre &agrave; z&eacute;ro.
	 */
	public void reset(String page) {
		if (page.equals("category"))
			((SettingsContainerMenuModel) this.model).resetCategory();
		((SettingsContainerMenuView) this.view).update();
	}
	
	/**
	 * Sauvegarde la liste de cat&eacute;ries. 
	 * @param page Page
	 * @param arrayList La liste de cat&eacute;ries.
	 */
	public void save(String page, ArrayList<String> arrayList) {
		if (page.equals("category")) {
			((SettingsContainerMenuModel) this.model).saveCategory(arrayList);
		}
	}
	
	/**
	 * Change la cat&eacute;gorie.
	 * @param text Nouveau nom de la cat&eacute;gorie.
	 * @param index Indice de la cat&eacute;gorie.
	 */
	public void changeCategory(String text, int index) {
		((SettingsContainerMenuModel) this.model).changeCategory(text, index);
		((SettingsContainerMenuView) this.view).update();
	}

	public void save(String page, HashMap<String, String> map) {
		((SettingsContainerMenuModel) this.model).updateGeneralSettings(map);
	}

	public void addCategory(String text) {
		((SettingsContainerMenuModel) this.model).addCategory(text);
	}
}
