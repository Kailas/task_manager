package fr.um.projet.view;

import java.awt.Color;

import javax.swing.BoxLayout;

import fr.um.projet.controller.ContainerMenuController;
import fr.um.projet.model.ContainerMenuModel;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class ContainerMenuView extends ContainerView {

	private static final long serialVersionUID = -3351635304579671068L;
	protected ContainerMenuController controllerCM;
	protected ContainerMenuModel modelCM;
	protected Menu menu;
	
	public ContainerMenuView(ContainerMenuController controller, ContainerMenuModel model) {
		super(controller, model);
		this.controllerCM = controller;
		this.modelCM = model;
		init();
	}
	
	private void init() {
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		this.setBackground(Color.YELLOW);
		menu = new Menu();
		menu.setBackground(Static.toColor(Resources.getConfig("secondaryMenuColor")));
		this.add(menu);
	}
	
	public void addButton(ButtonMenu button) {
		menu.addButton(button);
	}
}
