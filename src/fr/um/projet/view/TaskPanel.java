package fr.um.projet.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import fr.um.projet.Task;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class TaskPanel extends JPanel {
	
	private static final long serialVersionUID = 8875450738834602807L;

	protected JTextArea nameLabel;
	protected JLabel img3;
	protected MouseListener ml;
	protected boolean edit = false;
	protected JLabel removeLabel;
	protected JLabel finishLabel;
	protected boolean editable;
	protected boolean removable;
	protected boolean validable;
	
	public TaskPanel(Task task, boolean removable, boolean editable, boolean validable) {
		this.setLayout(null);
		this.editable = editable;
		this.removable = removable;
		this.validable = validable;
		this.setMinimumSize(new Dimension(400, 100));
		this.setPreferredSize(new Dimension(400, 100));
		this.setMaximumSize(new Dimension(400, 100));
		if (task.isFinished() && !edit) {
			this.setBackground(new Color(128, 255, 0));
		} else if (task.isLate()) {
			this.setBackground(new Color(255, 145, 178));
		} else {
			this.setBackground(Color.WHITE);
		}
		
		nameLabel = new JTextArea(task.getName());
		if (editable)
			img3 = new JLabel();
		if (removable)
			removeLabel = new JLabel();
		if (validable)
			finishLabel = new JLabel();
		if (editable || removable || validable) {
		 	ml = new MouseListener() {
	
				@Override
				public void mouseClicked(MouseEvent arg0) {
				}
	
				@Override
				public void mouseEntered(MouseEvent arg0) {
					if (editable)
						img3.setVisible(true);
					if (removable)
						removeLabel.setVisible(true);
					if (validable)
						finishLabel.setVisible(true);
				}
	
				@Override
				public void mouseExited(MouseEvent arg0) {
					if (editable)
						img3.setVisible(false);
					if (removable)
						removeLabel.setVisible(false);
					if (validable)
						finishLabel.setVisible(false);
				}
	
				@Override
				public void mousePressed(MouseEvent arg0) {
				}
	
				@Override
				public void mouseReleased(MouseEvent arg0) {
				}
				
			};
			// Pour eviter que l'icon disparaisse puisque l'event du parent ne s'applique pas aux enfants
			if (editable)
				img3.addMouseListener(ml);
			if (removable)
				removeLabel.addMouseListener(ml);
			if (validable)
				finishLabel.addMouseListener(ml);
		}
		
		nameLabel.setPreferredSize(new Dimension(320, 40));
		nameLabel.setLineWrap(true);
		nameLabel.setEditable(false);
		nameLabel.setBounds(40, 40, 320, 40);
		nameLabel.addMouseListener(ml);
		nameLabel.setBackground(this.getBackground());
		

		JLabel time = new JLabel(Static.compareDate(Static.getToday(), task.getDate()) + " restant(s)");
		time.setFont(new Font(Resources.getConfig("police"), Font.PLAIN, 11));
		time.setBounds(getPreferredSize().width - 100, 22, 100, 25);
		time.setBackground(Color.GREEN);
		
		this.addMouseListener(ml);
		this.add(time);
		this.add(datePanel((new SimpleDateFormat(Resources.getConfig("dateFormat"))).format(Static.stringToDate(task.getDate()).getTime())));
		this.add(categoryPanel(task.getCategory()));
		this.add(nameLabel);
		if (editable) {
			this.add(img3);
		}
		if (removable) {
			this.add(removeLabel);
		}
		if (validable) {
			this.add(finishLabel);
		}
		if (task.getType().equals("long") && edit == false) {
			addProgressBar(task.getProgress());
		}
		
	}
	
	protected JPanel datePanel(String date) {
		//(new SimpleDateFormat(Resources.getConfig("dateFormat"))).format(Static.stringToDate(task.getDate()).getTime())
		JPanel datePanel = new JPanel() {

			private static final long serialVersionUID = -5886456018798659510L;

			@Override 
			protected void paintComponent(Graphics g) {
				g.setColor(getBackground());
				g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
			}
		};
		JLabel img = new JLabel();
		JLabel dateLabel = new JLabel(date);
		
		datePanel.setPreferredSize(new Dimension(100, 30));
		datePanel.setBackground(new Color(250, 128, 114));
		datePanel.add(img);
		datePanel.add(dateLabel);
		datePanel.setBounds(296, 4, 100, 25);
		
		img.setIcon(Static.getIcon("horloge"));
		
		dateLabel.setForeground(Color.WHITE);
		
		return datePanel;
	}
	
	protected JPanel categoryPanel(String category) {
		JPanel catPanel = new JPanel() {
			private static final long serialVersionUID = 189273535582047329L;

			@Override 
			protected void paintComponent(Graphics g) {
				g.setColor(getBackground());
				g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
			}
		};
		JLabel img2 = new JLabel();
		JLabel catLabel = new JLabel(category);
		
		catPanel.setLayout(null);
		catPanel.setPreferredSize(new Dimension(100, 30));
		catPanel.setBackground(new Color(255, 203, 96));
		catPanel.add(img2);
		catPanel.add(catLabel);
		catPanel.setBounds(4, 4, 100, 25);
		
		
		img2.setIcon(Static.getIcon("dossier"));
		img2.setBounds(4, catPanel.getHeight() / 2 - 8, 16, 16);
		
		if (editable) {
			img3.setIcon(Static.getIcon("edit"));
			img3.setBounds(192, 8, 16, 16);
			img3.setVisible(false);
		}
		if (removable) {	
			removeLabel.setIcon(Static.getIcon("delete"));
			removeLabel.setBounds(120, 8, 16, 16);
			removeLabel.setVisible(false);
		}
		if (validable) {	
			finishLabel.setIcon(Static.getIcon("check"));
			finishLabel.setBounds(264, 8, 16, 16);
			finishLabel.setVisible(false);
		}

		catLabel.setForeground(Color.WHITE);
		catLabel.setBounds(24, catPanel.getHeight() / 2 - 12 / 2, 100, 12);
		
		return catPanel;
	}
	
	protected void addProgressBar(int value) {
		JPanel pan = new JPanel();
		pan.setLayout(null);
		pan.setPreferredSize(new Dimension(392, 20));
		pan.setBackground(getBackground());
		value *= 4;
		JProgressBar pg = new JProgressBar();
		pg.setBounds(0, 5, 95, 10);
		pg.setValue(value);
		pg.setForeground(Color.RED);
		pan.add(pg);
		
		pg = new JProgressBar();
		pg.setBounds(99, 5, 95, 10);
		pg.setValue(value - 100);
		pg.setForeground(Color.ORANGE);
		pan.add(pg);
		
		pg = new JProgressBar();
		pg.setBounds(198, 5, 95, 10);
		pg.setValue(value - 200);
		pg.setForeground(Color.YELLOW);
		pan.add(pg);
		
		pg = new JProgressBar();
		pg.setBounds(297, 5, 95, 10);
		pg.setValue(value - 300);
		pg.setForeground(Color.GREEN);
		pan.add(pg);
		pan.setBounds(4, 80, 392, 20);
		this.add(pan);
		this.revalidate();
	}
	
	protected void removeProgressBar() {
		this.remove(this.getComponentCount() - 1);
		revalidate();
		repaint();
	}
	
	public void editTask(MouseListener listener) {
		if (editable) {
			img3.addMouseListener(listener);
		}
	}
	
	public void addModifyListener(MouseListener listener) {
		if (editable) {
			img3.addMouseListener(listener);
		}
	}
	
	public void finishTaskListener(MouseListener listener) {
		if (validable) {
			finishLabel.addMouseListener(listener);
		}
	}
	
	public void removeTaskListener(MouseListener listener) {
		if (removable) {
			removeLabel.addMouseListener(listener);
		}
	}
	
}
