package fr.um.projet.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.toedter.calendar.JCalendar;

import fr.um.projet.controller.TaskManagerController;
import fr.um.projet.model.TaskManagerModel;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class TaskManagerView extends ContainerView {
	
	private static final long serialVersionUID = -7582900511079216527L;
	
	private JPanel menuGroupPanel;
	private ButtonMenu buttonSelected;
	private JPanel panel;
	//private JPanel commentPanel;
	private JPanel buttonPanel;
	
	private ProgressBar progressBar;
	
	private JCalendar calendarEnd;
	private JCalendar calendarBegin;
	private JTextArea nameTextArea;
	
	private ButtonMenu resetButton;
	private ButtonMenu validButton;
	
	private boolean enteredProgressBar;
	private int valueProgressBar;
	
	private TaskManagerModel tmModel;
	
	JPanel calendarBeginPanel = new JPanel();
	JPanel calendarEndPanel = new JPanel();
	
	JRadioButton typeShort;
	JRadioButton typeLong;

	public TaskManagerView(TaskManagerController controller, TaskManagerModel model) {
		super(controller, model);
		
		menuGroupPanel = new JPanel();	
		panel = new JPanel();
		buttonPanel = new JPanel();
		calendarEnd = new JCalendar();
		calendarBegin = new JCalendar();
		nameTextArea = new JTextArea(6, 40);
		Color color = Static.toColor(Resources.getConfig("buttonColor"));
		Color colorBg = Static.toColor(Resources.getConfig("buttonBgColor"));
		resetButton = new ButtonMenu(Resources.getString("resetButton"), color, colorBg);
		validButton = new ButtonMenu(Resources.getString("validButton"), color, colorBg);
		calendarBeginPanel = new JPanel();
		calendarEndPanel = new JPanel();
		buttonSelected = null;
		tmModel = (TaskManagerModel) model;
		
		menuGroupPanel.setLayout(new BoxLayout(menuGroupPanel, BoxLayout.Y_AXIS));
		nameTextArea.setEditable(true);
		nameTextArea.setLineWrap(true);

		resetButton.setPreferredSize(new Dimension(200, 40));
		resetButton.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				reset();
				revalidate();
			}
			
		});
		validButton.setPreferredSize(new Dimension(200, 40));
		validButton.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!getNameTask().isEmpty())
					((TaskManagerController) controller).addTask(getNameTask(), getTypeTask(), getCategoryTask(), getDateBeginTask(), getDateEndTask(), getPercentTask());
			}
			
		});
		buttonPanel.setMaximumSize(new Dimension(10000, 30));
		
		
		setLayout(new BorderLayout());

		panel.setMinimumSize(null);
		panel.setPreferredSize(null);
		panel.setMaximumSize(null);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel pan0 = new JPanel();
		pan0.setLayout(new BorderLayout());
		
		panel.add(settingsPanel());
		panel.add(titlePanel());
		panel.add(calendars());
		pan0.add(new JScrollPane(panel), BorderLayout.CENTER);
		pan0.add(buttonPanel, BorderLayout.SOUTH);
		
		buttonPanel.add(resetButton);
		buttonPanel.add(validButton);
		
		this.setLayout(new BorderLayout());
		JPanel groupPanel = new JPanel();
		groupPanel.setLayout(new BoxLayout(groupPanel, BoxLayout.Y_AXIS));
		groupPanel.add(new JScrollPane(menuGroupPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
		groupPanel.add(addGroupPanel());
		updateCategories();
		
		
		nameTextArea.setText(tmModel.getName());
		for (int k = 0; k < menuGroupPanel.getComponentCount(); ++k) {
			JPanel gp = ((JPanel) menuGroupPanel.getComponent(k));
			ButtonMenu bm = ((ButtonMenu) gp.getComponent(0));
			assert(bm != null) : "bm == null";
			assert(bm.getText() != null) : "bm == null";
			if (bm != null && bm.getText().equals(tmModel.getCategory())) {
				this.buttonSelected = bm;
				bm.setSelected(true);
			}
		}
		Calendar d = Static.stringToDate(tmModel.getDateBegin());
		calendarBegin.setCalendar(d);
		d = Static.stringToDate(tmModel.getDateEnd());
		calendarEnd.setCalendar(d);
		
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		this.add(groupPanel);
		this.add(pan0);
	}
	
	private JPanel settingsPanel() {
		JPanel settingsPanel = new JPanel();
		JPanel radioPanel = new JPanel();
		typeShort = new JRadioButton(Resources.getString("shortType"));
		typeLong = new JRadioButton(Resources.getString("longType"));
		ButtonGroup bg = new ButtonGroup();
		JPanel pan = new JPanel();
		progressBar = new ProgressBar();
		Dimension dimSettingsPanel = new Dimension(500, 50);

		radioPanel.setLayout(new BoxLayout(radioPanel, BoxLayout.X_AXIS));
		radioPanel.setOpaque(false);
		radioPanel.add(typeShort);
		radioPanel.add(typeLong);
		typeShort.setOpaque(false);
		typeShort.setSelected(!tmModel.getType().equals("long"));
		typeShort.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					progressBar.setVisible(false);
					calendarBeginPanel.setVisible(false);
				} else {
					progressBar.setVisible(true);
					calendarBeginPanel.setVisible(true);
				}
				progressBar.revalidate();
			}
			
		});
		
		typeLong.setSelected(tmModel.getType().equals("long"));
		typeLong.setOpaque(false);
		if (tmModel.getType().equals("long")) {
			typeLong.setSelected(true);
		}
		
		bg.add(typeShort);
		bg.add(typeLong);
		
		pan.setLayout(new FlowLayout());
		pan.add(progressBar);
		
		progressBar.setValue(tmModel.getPercent());
		progressBar.setMinValue(progressBar.getValue());
		progressBar.setVisible(tmModel.getType().equals("long"));
		progressBar.setStringPainted(true);
		progressBar.setPreferredSize(new Dimension(400, 20));
		valueProgressBar = tmModel.getPercent();
		progressBar.addMouseListener(new MouseListener() {
			
			
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				valueProgressBar = arg0.getX() * 100 / progressBar.getPreferredSize().width;
				if (valueProgressBar <= progressBar.getMinValue())
					return;
				progressBar.setValue(valueProgressBar);
				progressBar.revalidate();
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				enteredProgressBar = true;
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				enteredProgressBar = false;
				enteredProgressBar = false;
				progressBar.setValue(valueProgressBar);
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}
			
		});
		progressBar.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent arg0) {
				
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
				if (!enteredProgressBar) 
					return;
				int v = arg0.getX() * 100 / progressBar.getPreferredSize().width;
				if (v <= progressBar.getMinValue())
					return;
				progressBar.setValue(v);
			}
			
		});
		
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
		settingsPanel.add(radioPanel);
		settingsPanel.add(pan);
		settingsPanel.setMinimumSize(dimSettingsPanel);
		settingsPanel.setPreferredSize(dimSettingsPanel);
		settingsPanel.setMaximumSize(dimSettingsPanel);
		
		return settingsPanel;
	}
	
	private JPanel titlePanel() {
		JPanel titlePanel = new JPanel();
		JPanel p1 = new JPanel();
		Dimension dimTitlePanel = new Dimension(500, 150);
		JPanel p = new JPanel();

		p1.add(new JLabel(Resources.getString("textNewTaskLabel")));
		
		titlePanel.setLayout(new BorderLayout());
		titlePanel.add(p1, BorderLayout.NORTH);
		titlePanel.add(p, BorderLayout.CENTER);
		
		p.add(nameTextArea);
		
		titlePanel.setMinimumSize(dimTitlePanel);
		titlePanel.setPreferredSize(dimTitlePanel);
		titlePanel.setMaximumSize(dimTitlePanel);
		
		return titlePanel;
	}

	private JPanel calendars() {
		JPanel calendarPanel = new JPanel();
		JPanel cbp = new JPanel();
		JPanel cep = new JPanel();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		calendarPanel.add(calendarBeginPanel);
		calendarPanel.add(calendarEndPanel);
		
		calendarBegin.setLocale(Static.getLocale());
		calendarBegin.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				calendarEnd.setMinSelectableDate(new Date(calendarBegin.getDate().getTime() + 24 * 60 * 60 * 1000));
				calendarEnd.setDate(new Date(calendarBegin.getDate().getTime() + 24 * 60 * 60 * 1000));
				calendarEnd.revalidate();
			}
			
		});
		
		calendarBeginPanel.setVisible(tmModel.getType().equals("long"));
		calendarBeginPanel.setLayout(new BorderLayout());
		calendarBeginPanel.add(cbp, BorderLayout.NORTH);
		calendarBeginPanel.add(calendarBegin, BorderLayout.CENTER);
		
		
		calendarEnd.setLocale(Static.getLocale());
		
		calendarEndPanel.setLayout(new BorderLayout());
		calendarEndPanel.add(cep, BorderLayout.NORTH);
		calendarEndPanel.add(calendarEnd, BorderLayout.CENTER);

		cbp.add(new JLabel(Resources.getString("beginLabel") + " :"));
		cep.add(new JLabel(Resources.getString("endLabel") + " :"));
		try {
			calendarBegin.setMinSelectableDate(format.parse((calendarBegin.getDayChooser().getDay())
					+ "/" + (calendarBegin.getMonthChooser().getMonth() + 1) + "/" 
					+ calendarBegin.getYearChooser().getYear() ));
			calendarEnd.setMinSelectableDate(calendarBegin.getMinSelectableDate());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		return calendarPanel;
	}
	
	private JPanel addGroupPanel() {
		JPanel panel = new JPanel();
		JTextField tf = new JTextField();
		JButton button = new JButton();
		
		button.setIcon(Static.getIcon("add"));
		button.setPreferredSize(new Dimension(20, 20));
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (tf.getText().isEmpty())
					return;
				((TaskManagerController) controller).addCategory(tf.getText());
				updateCategories();
			}
			
		});
		
		tf.setPreferredSize(new Dimension(160, 20));
		
		panel.setOpaque(false);
		panel.setMinimumSize(new Dimension(200, 30));
		panel.setPreferredSize(new Dimension(200, 30));
		panel.setMaximumSize(new Dimension(200, 30));
		panel.add(tf);
		panel.add(button);
		
		return panel;
	}
	
	private void reset() {
		nameTextArea.setText("");
		progressBar.setValue(0);
		calendarEnd.setDate(new Date());
		calendarBegin.setDate(new Date());
	}
	
	private void updateCategories(){
		menuGroupPanel.removeAll();
		for (String cat : ((TaskManagerModel) model).getCategories()) {
			ButtonMenu bm = new ButtonMenu(cat, Static.toColor(Resources.getConfig("secondaryMenuButtonColor")));
			bm.setPreferredSize(new Dimension(200, 50));
			bm.addActionClicked(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (bm != buttonSelected) {
						if (buttonSelected != null)
							buttonSelected.setSelected(false);
						bm.setSelected(true);
						buttonSelected = bm;
					} else {
						bm.setSelected(false);
						buttonSelected = null;
					}
				}
				
			});
			if (cat.equals(((TaskManagerModel) this.model).getCategorySelected())) {
				if (buttonSelected != null)
					buttonSelected.setSelected(false);
				bm.setSelected(true);
				buttonSelected = bm;
			}
			JPanel panelTemp = new JPanel();
			panelTemp.setLayout(new BoxLayout(panelTemp, BoxLayout.X_AXIS));
			panelTemp.setMinimumSize(new Dimension(200, 50));
			panelTemp.setPreferredSize(new Dimension(200, 50));
			panelTemp.setMaximumSize(new Dimension(200, 50));
			panelTemp.add(bm);
			menuGroupPanel.add(panelTemp);
		}
		menuGroupPanel.revalidate();
	}
	
	public String getNameTask() { return nameTextArea.getText(); }
	
	private String getCategoryTask() { return buttonSelected == null ? "" : buttonSelected.getText(); }
	
	private String getDateBeginTask() { return calendarBegin.getDayChooser().getDay() + "/" + (calendarBegin.getMonthChooser().getMonth() + 1) + "/" + calendarBegin.getYearChooser().getYear(); }

	private String getDateEndTask() { return calendarEnd.getDayChooser().getDay() + "/" + (calendarEnd.getMonthChooser().getMonth() + 1) + "/" + calendarEnd.getYearChooser().getYear(); }

	private String getTypeTask() { return typeShort.isSelected() ? "short" : "long"; }

	private int getPercentTask() { return typeShort.isSelected() ? 0 : valueProgressBar; };
}
