package fr.um.projet.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Menu extends JPanel {
	
	private static final long serialVersionUID = 652601085531389100L;
	private ArrayList<ButtonMenu> buttons;
	
	public Menu() {
		setLayout(new FlowLayout());
		((FlowLayout) getLayout()).setVgap(0);
		buttons = new ArrayList<ButtonMenu>();
		setMinimumSize(new Dimension(250, 0));
		setPreferredSize(new Dimension(250, 0));
		setMaximumSize(new Dimension(250, 10000));
	}
	
	public void addButton(ButtonMenu button) {
		if (buttons.isEmpty()) {
			button.setSelected(true);
			//button.runActionListener();
		}
		buttons.add(button);
		button.setSelectable(true);
		button.setPreferredSize(new Dimension(250, 50));
		add(button);
		button.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (ButtonMenu b : buttons) {
					if (b != button) {
						b.setSelected(false);
					}
				}
			}
			
		});
	}

	public String getSelectedName() {
		for (ButtonMenu b : buttons) {
			if (b.isSelected()) {
				return b.getText();
			}
		}
		return null;
	}
	
	public ButtonMenu getButton(String name) {
		for (ButtonMenu b : buttons) {
			if (b.getText().equals(name))
				return b;
		}
		return null;
	}
	
	public ArrayList<ButtonMenu> getButtons() {
		return buttons;
	}
}
