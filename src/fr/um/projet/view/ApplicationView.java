package fr.um.projet.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.um.projet.controller.ApplicationController;
import fr.um.projet.model.ApplicationModel;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class ApplicationView extends JFrame {

	private static final long serialVersionUID = 7949128062665206094L;

	private ApplicationController controller;
	private ApplicationModel model;
	private Menu menuPanel;
	
	public ApplicationView(ApplicationController controller, ApplicationModel model) {
		System.setProperty( "file.encoding", "UTF-8" );
		this.setIconImage(Static.getIcon("snow").getImage());
		this.controller = controller;
		this.model = model;
		this.setResizable(Boolean.valueOf(Resources.getConfig("resizable")));
		this.setTitle(Resources.getString("title"));
		this.setSize(
			Integer.parseInt(Resources.getConfig("width")), 
			Integer.parseInt(Resources.getConfig("height"))
		);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.createMenuPanel();
		
		JPanel pan = new JPanel();
		pan.setPreferredSize(new Dimension(200, 600));
		pan.setLayout(new BoxLayout(pan, BoxLayout.LINE_AXIS));
		menuPanel.setBackground(Static.toColor(Resources.getConfig("generalMenuColor")));
		pan.add(menuPanel);

		pan.add(model.getContainer());
		this.setContentPane(pan);
		this.setVisible(true);
	}
	
	/**
	 * Cree le menu principal.
	 */
	private void createMenuPanel() {
		this.menuPanel = new Menu();
		Color buttonColor = Static.toColor(Resources.getConfig("generalMenuButtonColor"));
		ButtonMenu bm1 = new ButtonMenu(Resources.getString("tasksMenuButton"), buttonColor);
		ButtonMenu bm2 = new ButtonMenu(Resources.getString("newTaskMenuButton"), buttonColor);
		ButtonMenu bm4 = new ButtonMenu(Resources.getString("bilanMenuButton"), buttonColor);
		ButtonMenu bm3 = new ButtonMenu(Resources.getString("settingsMenuButton"), buttonColor);
		bm1.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.menuAction("tasks");
				repaint();
				revalidate();
			}
			
		});
		bm2.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.menuAction("newTask");
			}
			
		});
		bm3.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.menuAction("settings");
			}
			
		});
		bm4.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.menuAction("bilan");
			}
			
		});
		this.menuPanel.addButton(bm1);
		this.menuPanel.addButton(bm2);
		this.menuPanel.addButton(bm4);
		this.menuPanel.addButton(bm3);
	}
	
	public void update() {
		this.getContentPane().remove(this.getContentPane().getComponentCount() - 1);
		this.add(this.model.getContainer());
		this.revalidate();
	}
	
	public void updateButton() {
		for (ButtonMenu bm : menuPanel.getButtons()) {
			bm.setSelected(false);
		}
		menuPanel.getButton(Resources.getString("tasksMenuButton")).setSelected(true);
	}
}