package fr.um.projet.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class ButtonMenu extends JPanel implements MouseListener {
	
	private static final long serialVersionUID = -3289330750192244942L;
	
	private JLabel textLabel;
	private boolean selected;
	private ArrayList<ActionListener> listener;
	private Color colorHover;
	private Color colorBg;
	private boolean selectable;
	private JLabel icon1;
	private JLabel icon2;
	
	public ButtonMenu(String text, Color colorHover, Color colorBg) {
		this.colorBg = colorBg;
		selectable = false;
		listener = new ArrayList<ActionListener>();
		textLabel = new JLabel(text);
		textLabel.setForeground(Static.toColor(Resources.getConfig("textMenuColor")));
		textLabel.setFont(new Font("Terminal", Font.BOLD, 20));
		setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
		selected = false;
		this.colorHover = colorHover;
		if (this.colorBg == null) {
			this.setBackground(colorHover);
			this.setOpaque(false);
		} else {
			this.setBackground(colorBg);
		}
		
		this.addMouseListener(this);
		
		//
		icon1 = new JLabel(Static.getIcon("so"));
		icon2 = new JLabel(Static.getIcon("so2"));
		//

		icon1.setVisible(false);
		icon2.setVisible(false);
		
		this.add(icon1);
		this.add(textLabel, gbc);
		this.add(icon2);
	}
	
	public ButtonMenu(String text, Color colorHover) {
		this(text, colorHover, null);
	}
	
	public void setSelectable(boolean b) {
		selectable = b;
	}
	
	public boolean isSelectable() {
		return selectable;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		colorHover(selected);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		colorHover(true);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (!selected)
			colorHover(false);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (selectable)
			setSelected(true);
		runActionListener();
	}
	
	public void addActionClicked(ActionListener listener) {
		this.listener.add(listener);
	}
	
	public void runActionListener() {
		for (ActionListener l : listener)
			l.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}
	
	public String getText() {
		return textLabel.getText();
	}
	
	private void colorHover(boolean state) {
		if (state) {
			if (Resources.getConfig("xmas").equals("false")) {
				if (colorBg == null) {
					setOpaque(true);
				} else {
					setBackground(colorHover);
				}
			} else {
				icon1.setVisible(true);
				icon2.setVisible(true);
			}
		} else {
			if (Resources.getConfig("xmas").equals("false")) {
				if (colorBg == null) {
					setOpaque(false);
				} else {
					setBackground(colorBg);
				}
			} else {
				icon1.setVisible(false);
				icon2.setVisible(false);
			}
		}
		revalidate();
		repaint();
	}
}
