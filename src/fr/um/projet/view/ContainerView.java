package fr.um.projet.view;

import javax.swing.JPanel;

import fr.um.projet.controller.ContainerController;
import fr.um.projet.model.ContainerModel;

public class ContainerView extends JPanel {

	private static final long serialVersionUID = -2668498714333999935L;
	
	protected ContainerModel model;
	protected ContainerController controller;
	
	public ContainerView(ContainerController controller, ContainerModel model) {
		this.model = model;
		this.controller = controller;
	}
	
}
