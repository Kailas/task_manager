package fr.um.projet.view;

import javax.swing.JProgressBar;

public class ProgressBar extends JProgressBar {

	private static final long serialVersionUID = 7049153841004248739L;
	private int minValue;
	
	public ProgressBar() {
		super();
		minValue = 0;
	}
	
	/**
	 * Sp&eacute;cifie la valeur minimale de la progression.
	 * @param min La valeur minimale de la progression.
	 */
	public void setMinValue(int min) {
		this.minValue = min;
	}
	
	/**
	 * Retourne la valeur minimale de la progression.
	 * @return La valeur minimale de la progression.
	 */
	public int getMinValue() {
		return minValue;
	}
}
