package fr.um.projet.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.NumberFormatter;

import fr.um.projet.controller.ContainerMenuController;
import fr.um.projet.controller.SettingsContainerMenuController;
import fr.um.projet.model.ContainerMenuModel;
import fr.um.projet.model.SettingsContainerMenuModel;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class SettingsContainerMenuView extends ContainerMenuView {

	private static final long serialVersionUID = 4235201538303871944L;
	
	private JPanel container;
	private ButtonMenu resetButton;
	private ButtonMenu validButton;
	private HashMap<String, String> generalSettings;

	public SettingsContainerMenuView(ContainerMenuController controller, ContainerMenuModel model) {
		super(controller, model);
		container = new JPanel();
		createSettingsMenu();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		generalSettings = new HashMap<String, String>();
		
		Color c = Static.toColor(Resources.getConfig("buttonColor"));
		Color cBg = Static.toColor(Resources.getConfig("buttonBgColor"));
		resetButton = new ButtonMenu(Resources.getString("resetButton"), c, cBg);
		validButton = new ButtonMenu(Resources.getString("validButton"), c, cBg);
		resetButton.setPreferredSize(new Dimension(200, 40));
		resetButton.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				((SettingsContainerMenuController) controller).reset(((SettingsContainerMenuModel) model).getPage());
			}
			
		});
		validButton.setPreferredSize(new Dimension(200, 40));
		validButton.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (((SettingsContainerMenuModel) model).getPage().equals("category"))
					((SettingsContainerMenuController) controller).save(((SettingsContainerMenuModel) model).getPage(), getNewCategories());
				else
					((SettingsContainerMenuController) controller).save(((SettingsContainerMenuModel) model).getPage(), generalSettings);
			}
			
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.setMaximumSize(new Dimension(10000, 30));
		buttonPanel.add(resetButton);
		buttonPanel.add(validButton);
		JPanel p = new JPanel();
		
		p.setLayout(new BorderLayout());
		p.add(new JScrollPane(container), BorderLayout.CENTER);
		p.add(buttonPanel, BorderLayout.SOUTH);
		this.add(p);
	}
	
	private void createSettingsMenu() {
		Color color = Static.toColor(Resources.getConfig("secondaryMenuButtonColor"));
		ButtonMenu categoryButton = new ButtonMenu(Resources.getString("categorySettingsMenuButton"), color);
		ButtonMenu generalButton = new ButtonMenu(Resources.getString("generalSettingsMenuButton"), color);
		categoryButton.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (((SettingsContainerMenuModel) model).getPage().equals("category")) {
					return;
				}
				((SettingsContainerMenuController) controller).buttonAction("category");
				container.repaint();
			}
			
		});
		generalButton.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (((SettingsContainerMenuModel) model).getPage().equals("general")) {
					return;
				}
				((SettingsContainerMenuController) controller).buttonAction("general");
			}
			
		});
		
		super.addButton(categoryButton);
		super.addButton(generalButton);
		categoryButton.repaint();
		categoryButton.revalidate();
	}
	
	private void createCategoryPanel() {
		String cat;
		for (int k = 0;  k < ((SettingsContainerMenuModel) model).getCategories().size(); ++k) {
			cat = ((SettingsContainerMenuModel) model).getCategories().get(k);
			JPanel p = new JPanel();
			JTextField tf = new JTextField();
			JButton del = new JButton();

			tf.setText(cat);
			del.setMinimumSize(new Dimension(20, 20));
			del.setPreferredSize(new Dimension(20, 20));
			del.setMaximumSize(new Dimension(20, 20));
			del.setIcon(Static.getIcon("delete"));
			final int index = k;
			del.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					((SettingsContainerMenuController) controller).deleteCategory(index);
					//container.removeAll();
					//createCategoryPanel();
					p.setVisible(false);
					tf.setText("");
					container.revalidate();
					container.repaint();
				}
				
			});
			// Peut etre a ameliorer
			tf.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					((SettingsContainerMenuController) controller).changeCategory(tf.getText(), index);
				}
				
			});
			p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
			p.setMinimumSize(new Dimension(0, 20));
			p.setPreferredSize(new Dimension(100, 20));
			p.setMaximumSize(new Dimension(1000, 20));
			p.setBackground(Color.BLUE);

			p.add(tf);
			p.add(del);
			container.add(p);
		}
		JPanel p = new JPanel();
		JTextField tf = new JTextField();
		JButton add = new JButton();

		tf.setText("");
		add.setMinimumSize(new Dimension(20, 20));
		add.setPreferredSize(new Dimension(20, 20));
		add.setMaximumSize(new Dimension(20, 20));
		add.setIcon(Static.getIcon("add"));
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (tf.getText().isEmpty())
					return;
				((SettingsContainerMenuController) controller).addCategory(tf.getText());
				container.removeAll();
				createCategoryPanel();
				container.revalidate();
				container.repaint();
			}
			
		});
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setMinimumSize(new Dimension(0, 20));
		p.setPreferredSize(new Dimension(100, 20));
		p.setMaximumSize(new Dimension(1000, 20));
		p.setBackground(Color.BLUE);

		p.add(tf);
		p.add(add);
		container.add(p);

	}
	
	private void createSettingsPanel() {
		JComboBox<String> langCB = new JComboBox<String>(new String[] {"en", "fr"});
		langCB.setSelectedItem(Resources.getConfig("lang"));
		Dimension dimLang = new Dimension(60, 20);
		langCB.setMinimumSize(dimLang);
		langCB.setPreferredSize(dimLang);
		langCB.setMaximumSize(dimLang);
		langCB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				generalSettings.put("lang", (String) langCB.getSelectedItem());
			}
			
		});
		container.add(addSetting(Resources.getString("lang"), langCB));
		JComboBox<String> dateCB = new JComboBox<String>(new String[] {"MM/dd/yyyy", "dd/MM/yyyy"});
		dateCB.setSelectedItem(Resources.getConfig("dateFormat"));
		Dimension dimDate = new Dimension(120, 20);
		dateCB.setMinimumSize(dimDate);
		dateCB.setPreferredSize(dimDate);
		dateCB.setMaximumSize(dimDate);
		dateCB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				generalSettings.put("dateFormat", (String) dateCB.getSelectedItem());
			}
			
		});
		container.add(addSetting(Resources.getString("dateFormat"), dateCB));
		
		Color c = Static.toColor(Resources.getConfig("generalMenuButtonColor"));
		container.add(addSettingColor("generalMenuButtonColor", c.getRed(), c.getGreen(), c.getBlue()));
		c = Static.toColor(Resources.getConfig("generalMenuColor"));
		container.add(addSettingColor("generalMenuColor", c.getRed(), c.getGreen(), c.getBlue()));
		c = Static.toColor(Resources.getConfig("secondaryMenuButtonColor"));
		container.add(addSettingColor("secondaryMenuButtonColor", c.getRed(), c.getGreen(), c.getBlue()));
		c = Static.toColor(Resources.getConfig("secondaryMenuColor"));
		container.add(addSettingColor("secondaryMenuColor", c.getRed(), c.getGreen(), c.getBlue()));
		c = Static.toColor(Resources.getConfig("buttonColor"));
		container.add(addSettingColor("buttonColor", c.getRed(), c.getGreen(), c.getBlue()));
		c = Static.toColor(Resources.getConfig("buttonBgColor"));
		container.add(addSettingColor("buttonBgColor", c.getRed(), c.getGreen(), c.getBlue()));
		container.add(addSettingXmas());
	}
	
	private JPanel addSettingXmas() {
		JCheckBox check = new JCheckBox();
		check.setSelected(Resources.getConfig("xmas").equals("true"));
		check.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				generalSettings.put("xmas", check.isSelected() ? "true" : "false");
			}
			
		});
		return addSetting(Resources.getString("xmas"), check);
	}
	
	private JPanel addSettingColor(String name, int cr, int cg, int cb) {
		JPanel p = new JPanel();
		JLabel tf = new JLabel(Resources.getString(name));
		Dimension dimLabel = new Dimension(250, 20);
		tf.setMinimumSize(dimLabel);
		tf.setPreferredSize(dimLabel);
		tf.setMaximumSize(dimLabel);
		
		NumberFormatter formatter = new NumberFormatter( NumberFormat.getInstance());
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(255);
		formatter.setCommitsOnValidEdit(true);
		
		JFormattedTextField r = new JFormattedTextField(formatter);
		JFormattedTextField g = new JFormattedTextField(formatter);
		JFormattedTextField b = new JFormattedTextField(formatter);
		
		DocumentListener al = new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {
				action();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				action();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				action();
			}
			
			public void action() {
				String c = Static.rgbToStringColor(r.getText(), g.getText(), b.getText());
				generalSettings.put(name, c);
			}
			
		};
		
		r.getDocument().addDocumentListener(al);
		g.getDocument().addDocumentListener(al);
		b.getDocument().addDocumentListener(al);
		
		Dimension dim = new Dimension(30, 20);
		r.setMinimumSize(dim);
		r.setPreferredSize(dim);
		r.setMaximumSize(dim);
		
		g.setMinimumSize(dim);
		g.setPreferredSize(dim);
		g.setMaximumSize(dim);
		
		b.setMinimumSize(dim);
		b.setPreferredSize(dim);
		b.setMaximumSize(dim);
		
		r.setValue(cr);
		g.setValue(cg);
		b.setValue(cb);
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setMinimumSize(new Dimension(0, 20));
		p.setPreferredSize(new Dimension(100, 20));
		p.setMaximumSize(new Dimension(1000, 20));
		
		p.add(tf);
		p.add(new JLabel("R "));
		p.add(r);
		p.add(new JLabel("  G "));
		p.add(g);
		p.add(new JLabel("  B "));
		p.add(b);
		
		return p;
	}
	
	private JPanel addSetting(String name, Component c) {
		JPanel p = new JPanel();
		JLabel tf = new JLabel(name);
		
		Dimension dimLabel = new Dimension(250, 20);
		tf.setMinimumSize(dimLabel);
		tf.setPreferredSize(dimLabel);
		tf.setMaximumSize(dimLabel);

		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setMinimumSize(new Dimension(0, 20));
		p.setPreferredSize(new Dimension(100, 20));
		p.setMaximumSize(new Dimension(1000, 20));
		
		p.add(tf);
		p.add(c);
		
		return p;
	}
	
	public void update() {
		if (((SettingsContainerMenuModel) this.model).getPage().equals("category")) {
			container.removeAll();
			createCategoryPanel();
			revalidate();
			repaint();
		} else if (((SettingsContainerMenuModel) this.model).getPage().equals("general")) {
			container.removeAll();
			generalSettings = ((SettingsContainerMenuModel) model).getGeneralSettings();
			createSettingsPanel();
			revalidate();
			repaint();
		}
	}
	
	private ArrayList<String> getNewCategories() {
		ArrayList<String> list = new ArrayList<String>();
		for (int k = 0; k < container.getComponentCount(); ++k) {
			JPanel p = (JPanel) container.getComponent(k);
			for (int j = 0; j < p.getComponentCount(); ++j) {
				if (p.getComponent(j) instanceof JTextField) {
					JTextField tf = (JTextField) p.getComponent(j);
					if (!tf.getText().isEmpty())
						list.add(tf.getText());
					else
						list.add(null);
				}
			}
		}
		return list;
	}
}
