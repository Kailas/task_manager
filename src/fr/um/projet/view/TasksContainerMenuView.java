package fr.um.projet.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.um.projet.Task;
import fr.um.projet.controller.TasksContainerMenuController;
import fr.um.projet.model.TasksContainerMenuModel;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class TasksContainerMenuView extends ContainerMenuView {

	private static final long serialVersionUID = -865716951447704199L;
	private JPanel panel;	// Contient les taches
	
	public TasksContainerMenuView(TasksContainerMenuController controller, TasksContainerMenuModel model) {
		super(controller, model);
		createTaskMenu();
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JScrollPane scrollPane = new JScrollPane(panel);
		this.setBackground(Color.GRAY);
		this.add(scrollPane);
		updateTasks();
	}
	
	public void createTaskMenu() {
		Color c = Static.toColor(Resources.getConfig("secondaryMenuButtonColor"));
		ButtonMenu taskMenuAll = new ButtonMenu(Resources.getString("allTaskMenuButton"), c);
		ButtonMenu taskMenuInProgress = new ButtonMenu(Resources.getString("inProgressTaskMenuButton"), c);
		ButtonMenu taskMenuLate = new ButtonMenu(Resources.getString("lateTaskMenuButton"), c);
		ButtonMenu taskMenuFinished = new ButtonMenu(Resources.getString("finishTaskMenuButton"), c);
		taskMenuAll.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				((TasksContainerMenuController) controller).buttonAction("all");
			}
			
		});
		taskMenuInProgress.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				((TasksContainerMenuController) controller).buttonAction("progress");
			}
			
		});
		taskMenuLate.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				((TasksContainerMenuController) controller).buttonAction("late");
			}
			
		});
		taskMenuFinished.addActionClicked(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				((TasksContainerMenuController) controller).buttonAction("finish");
			}
			
		});
		
		super.addButton(taskMenuAll);
		super.addButton(taskMenuInProgress);
		super.addButton(taskMenuLate);
		super.addButton(taskMenuFinished);
		taskMenuInProgress.repaint();
		taskMenuInProgress.revalidate();
	}
	
	public void updateTasks() {
		panel.removeAll();
		for (Task t : ((TasksContainerMenuModel) this.model).getTasks()) {
			JPanel pan = new JPanel();
			TaskPanel tp = new TaskPanel(t, ((TasksContainerMenuModel) this.model).taskRemovable(), 
					((TasksContainerMenuModel) this.model).taskEditable(), ((TasksContainerMenuModel) this.model).taskValidable());
			tp.addModifyListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent arg0) {
					// Stock la tache a modifier
					Static.setTask(t);
					Static.changePage("newTask");
					
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					
				}
				
			});
			tp.finishTaskListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent arg0) {
					((TasksContainerMenuController) controller).finishTask(t);					
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					
				}
				
			});
			tp.removeTaskListener(new MouseListener() {
				
				@Override
				public void mouseClicked(MouseEvent arg0) {
					((TasksContainerMenuController) controller).removeTask(t);						
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					
				}
				
			});
			pan.setLayout(new FlowLayout());
			pan.setMinimumSize(tp.getMinimumSize());
			pan.setPreferredSize(tp.getPreferredSize());
			pan.setMaximumSize(tp.getMaximumSize());
			pan.add(tp);
			panel.add(pan);
		}
		panel.revalidate();
		panel.repaint();
	}
}
