package fr.um.projet.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JCalendar;

import fr.um.projet.Task;
import fr.um.projet.controller.ResultsContainerController;
import fr.um.projet.model.ResultsContainerModel;
import fr.um.projet.util.Resources;
import fr.um.projet.util.Static;

public class ResultsContainerView extends ContainerView {
	
	private static final long serialVersionUID = -2825117183060017564L;
	private JPanel calendarPanel;
	private JTabbedPane tabbedPane;

	public ResultsContainerView(ResultsContainerController controller, ResultsContainerModel model) {
		super(controller, model);
		this.setLayout(new BorderLayout());
		calendarPanel = new JPanel();
		calendarPanel.setLayout(new BoxLayout(calendarPanel, BoxLayout.Y_AXIS));
		
		JCalendar calendarBegin = new JCalendar();
		JCalendar calendarEnd = new JCalendar();
		
		calendarBegin.setLocale(Static.getLocale());
		calendarEnd.setLocale(Static.getLocale());
		
		calendarPanel.add(panelCalendar(calendarBegin, Resources.getString("beginLabel")));
		calendarPanel.add(panelCalendar(calendarEnd, Resources.getString("endLabel")));
		((ResultsContainerController) controller).setDate(Static.calendarToString(calendarBegin.getCalendar()), Static.calendarToString(calendarEnd.getCalendar()));
		add(calendarPanel, BorderLayout.WEST);
		
		
		calendarBegin.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				calendarEnd.setMinSelectableDate(calendarBegin.getDate());
				calendarEnd.revalidate();
				((ResultsContainerController) controller).setDate(Static.calendarToString(calendarBegin.getCalendar()), Static.calendarToString(calendarEnd.getCalendar()));
				update();
			}
			
		});
		
		calendarEnd.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				calendarEnd.setMinSelectableDate(calendarBegin.getDate());
				calendarEnd.revalidate();
				((ResultsContainerController) controller).setDate(Static.calendarToString(calendarBegin.getCalendar()), Static.calendarToString(calendarEnd.getCalendar()));
				update();
			}
			
		});
	}
	
	private JPanel panelCalendar(JCalendar calendar, String str) {
		JPanel panel = new JPanel();

		Dimension dim = new Dimension(250, 200);
		
		panel.setMinimumSize(dim);
		panel.setPreferredSize(dim);
		panel.setMaximumSize(dim);
		JLabel label = new JLabel(str);
		
		panel.add(label);
		panel.add(calendar);
		return panel;
	}
	
	private JPanel panelStats() {
		JPanel panel = new JPanel();
		int total = ((ResultsContainerModel) model).getTasks().size();
		int finished = 0;
		int inProgress = 0;
		int inLate = 0;
		if (total != 0) {
			finished = ((ResultsContainerModel) model).getTasksFinished().size() * 100 / total;
			inProgress = ((ResultsContainerModel) model).getTasksInProgress().size() * 100 / total;
			inLate = ((ResultsContainerModel) model).getTasksInLate().size() * 100 / total;
		}
		
		panel.add(panelPercent(Resources.getString("finishTaskMenuButton"), finished));
		panel.add(panelPercent(Resources.getString("inProgressTaskMenuButton"), inProgress));
		panel.add(panelPercent(Resources.getString("lateTaskMenuButton"), inLate));
		return panel;
	}
	
	private JPanel panelPercent(String name, int percent) {
		JPanel panel = new JPanel();
		JProgressBar pb = new JProgressBar();
		JLabel nameLabel = new JLabel(name);
		pb.setValue(percent);
		pb.setStringPainted(true);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JPanel textCenterPanel = new JPanel();
		textCenterPanel.add(nameLabel);
		panel.setPreferredSize(new Dimension(250, 50));
		panel.add(textCenterPanel);
		panel.add(pb);
		return panel;
	}
	
	private JTabbedPane panelsTasks() {
		JTabbedPane tab = new JTabbedPane();
		JTextArea ta = new JTextArea();
		ta.setEditable(false);
		tab.addTab(Resources.getString("allTaskMenuButton"), addTasks(((ResultsContainerModel) model).getTasks()));
		tab.addTab(Resources.getString("inProgressTaskMenuButton"), addTasks(((ResultsContainerModel) model).getTasksInProgress()));
		tab.addTab(Resources.getString("lateTaskMenuButton"), addTasks(((ResultsContainerModel) model).getTasksInLate()));
		tab.addTab(Resources.getString("finishTaskMenuButton"), addTasks(((ResultsContainerModel) model).getTasksFinished()));
		tab.addTab(Resources.getString("modifications"), new JScrollPane(ta));
		tab.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				// TODO Auto-generated method stub

				JTextArea p = ((JTextArea) ((JScrollPane) tabbedPane.getComponent(4)).getViewport().getView());
				p.setText("");
			}
			
		});
		return tab;
	}
	
	private JScrollPane addTasks(ArrayList<Task> tasks) {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		for (Task t : tasks) {
			JPanel pan = new JPanel();
			TaskPanel tp = new TaskPanel(t, false, false, false);
			pan.setLayout(new FlowLayout());
			pan.setMinimumSize(tp.getMinimumSize());
			pan.setPreferredSize(tp.getPreferredSize());
			pan.setMaximumSize(tp.getMaximumSize());
			pan.add(tp);
			panel.add(pan);
			tp.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent arg0) {
					tabbedPane.setSelectedIndex(4);
					JTextArea p = ((JTextArea) ((JScrollPane) tabbedPane.getComponent(4)).getViewport().getView());
					ArrayList<Task> tasks = ((ResultsContainerModel) model).getTasksModify(t.getId());
					if (tasks.size() < 2) {
						return;
					}
					String str = "";
					for (int k = 1; k < tasks.size(); ++k) {
						Task t2 = tasks.get(k - 1);
						Task t1 = tasks.get(k);
						str += t2.getWrite();
						if (!t1.getName().equals(t2.getName())) {
							str += "\n\tname : " + t2.getName();
						}
						if (!t1.getType().equals(t2.getType())) {
							str += "\n\ttype : " + t2.getType();
						}
						if (!t1.getCategory().equals(t2.getCategory())) {
							str += "\n\tcategory : " + t2.getCategory();
						}
						if (t1.getProgress() != t2.getProgress()) {
							str += "\n\tpercent : " + t2.getProgress();
						}
						if (!t1.getCreationDate().equals(t2.getCreationDate())) {
							str += "\n\tcreation date : " + t2.getCreationDate();
						}
						if (!t1.getDate().equals(t2.getDate())) {
							str += "\n\tdate : " + t2.getDate();
						}
						if (t1.isFinished() != t2.isFinished()) {
							str += "\n\tFinished : " + t2.isFinished();
						}
						str += "\n\n";
					}
					p.setText(str);
					tabbedPane.revalidate();
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					
					
				}
				
			});
		}
		return new JScrollPane(panel);
	}
	
	public void update() {
		if (getComponentCount() >= 2) {
			this.remove(2);
			this.remove(1);
		}
		tabbedPane = panelsTasks();
		add(panelStats(), BorderLayout.NORTH);
		add(tabbedPane, BorderLayout.CENTER);
		revalidate();
		repaint();
	}
}
