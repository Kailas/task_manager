package fr.um.projet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.um.projet.util.Static;

/**
 * Contient tous les &eacute;l&eacute;ments d&eacute;crivant une t&acirc;che.
 */
public class Task implements Serializable {
	
	private static final long serialVersionUID = -3796057681024242059L;
	private static ArrayList<Integer> idList;
	
	static {
		idList = new ArrayList<Integer>();
	};

	public static ArrayList<String> categories = new ArrayList<String>();
	
	protected String write;
	protected int id;
	protected String name;
	protected String creationDate;
	protected String date;
	protected String category;
	protected String type;
	protected int progress;
	protected boolean finished;
	protected Task previousTask;
	
	/**
	 * Cr&eacute;e un Task vide.
	 */
	public Task() {
		this((new SimpleDateFormat("dd/MM/yyyy|HH:mm:ss")).format(new Date()), findId(), "", "", "", "", 0, "", false);
	}
	
	public Task(String name) {
		this(read(name));
	}
	
	public Task(Task task) {
		this((new SimpleDateFormat("dd/MM/yyyy|HH:mm:ss")).format(new Date()), task.getId(), task.getName(), task.getDate(), task.getCategory(), task.getType(), task.getProgress(), task.getCreationDate(), task.isFinished());
	}
	
	public Task(String name, String date, String category, String type, int progress, String creationDate, boolean finished) {
		this((new SimpleDateFormat("dd/MM/yyyy|HH:mm:ss")).format(new Date()), findId(), name, date, category, type, progress, creationDate, finished);
	}
	
	/**
	 * Cr&eacute;e un task.
	 * @param write La date et heure de la cr&eacute;ation/midification de la t&acirc;che.
	 * @param id L'identifiant de la t&acirc;che.
	 * @param name Le nom de la t&acirc;che.
	 * @param date La date de fin de la .
	 * @param category La cat&eacute;gorie de la t&acirc;che.
	 * @param type Le type de la t&acirc;che.
	 * @param progress Le pourcentage de progression de la t&acirc;che.
	 * @param creationDate La date de d&eacute;but de la t&acirc;che.
	 * @param finished L'&eacute;tat de la t&acirc;che.
	 */
	private Task(String write, int id, String name, String date, String category, String type, int progress, String creationDate, boolean finished) {
		Task t = read(String.valueOf(id));
		if (t != null) {
			previousTask = t;
		}
		this.write = write;
		this.id = id;
		this.name = name;
		this.date = date;
		this.category = category;
		this.type = type;
		this.creationDate = creationDate;
		this.progress = progress;
		this.finished = finished;
		if (!categories.contains(category)) {
			categories.add(category);
		}
		if (!idList.contains(id))
			idList.add(id);
	}
	
	/**
	 * Retourne le nom de la t&acirc;che.
	 * @return Le nom de la t&acirc;che.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sp&eacute;cifie le nom de la t&acirc;che.
	 * @param name Le nom de la t&acirc;che.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retroune la date de fin de la t&acirc;che.
	 * @return La date de fin de la t&acirc;che.
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sp&eacute;cifie la date de fin de la t&acirc;che.
	 * @param date La date de fin de la t&acirc;che.
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * Sp&eacute;cifie la date de d&eacute;but de la t&acirc;che.
	 * @param date La date de d&eacute;but de la t&acirc;che.
	 */
	public void setCreationDate(String date) {
		this.creationDate = date;
	}
	
	/**
	 * Retourne la cat&eacute;tion de la t&acirc;che.
	 * @return La cat&eacute;gorie de la t&acirc;che.
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Sp&eacute;cifie la cat&eacute;tion de la t&acirc;che.
	 * @param category La cat&eacute;gorie de la t&acirc;che.
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
	/**
	 * Retourne le type de la t&acirc;che.
	 * @return Le type de la t&acirc;che.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sp&eacute;cifie le type de la t&acirc;che.
	 * @param type Le type de la t&acirc;che.
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Retourne le pourcentage de progression de la t&acirc;che.
	 * @return Le pourcentage de progression de la t&acirc;che.
	 */
	public int getProgress() {
		return progress;
	}

	/**
	 * Sp&eacute;cifie le pourcentage de progression de la t&acirc;che.
	 * @param progress Le pourcentage de progression de la t&acirc;che.
	 */
	public void setProgress(int progress) {
		this.progress = progress;
		if (progress == 100) {
			finished = true;
		}
	}
	
	/**
	 * Retourn l'identifiant de la t&acirc;che.
	 * @return L'identifiant de la t&acirc;che.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sp&eacute;cifie l'identifiant de la t&acirc;che.
	 * @param id L'identifiant de la t&acirc;che.
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Retoune la date de d&eacute;but de la t&acirc;che.
	 * @return La date de d&eacute;but de la t&acirc;che.
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Retourne true ou false suivant si la t&acirc;che est termin&eacute;e ou pas.
	 * @return true ou false suivant si la t&acirc;che est termin&eacute;e ou pas.
	 */
	public boolean isFinished() {
		return finished;
	}
	
	/**
	 * Sp&eacute;cifie si la t&acirc;che est termin&eacute;e ou pas.
	 * @param finished true ou false suivant si la t&acirc;che est termin&eacute;e ou pas.
	 */
	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	
	@Override
	public String toString() {
		return "Write : " + write + "\nId : " + id + "\nName : " + name + "\nCreation date : " + creationDate + "\nEnd Date : " + date + "\nCategory : " + category
				+ "\nType : " + type + "\nPercent : " + progress + "\nFinished : " + (finished ? "true" : false);
	}
	
	/**
	 * Retourne la date de la cr&eacute;tion/modification de la t&acirc;che.
	 * @return La date de la cr&eacute;tion/modification de la t&acirc;che.
	 */
	public String getWrite() {
		return write;
	}
	
	public boolean isLate() {
		if (type.equals("long")) {
			int nbTotal = Static.compareDate(creationDate, date);
			int nbToday = Static.compareDate(creationDate, Static.getToday());
			int p = (int) nbToday * 4 / nbTotal;
			return progress / 25 < p;
		}
		return Static.compareDateWithToday(date) >= 0;
	}
	
	public Task getPreviousTask() {
		return previousTask;
	}
	
	public void remove() {
		idList.remove(Integer.valueOf(id));
		File[] files = (new File("res/save")).listFiles();
		for (File f : files) {
			if (f.getName().replaceAll(".save", "").equals(String.valueOf(id))) {
				f.delete();
				return;
			}
		}
	}
	
	public void write() {
		File f = new File("res/save/" + id + ".save");
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(f));
			oos.writeObject(this);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static Task read(String name) {
		File f = new File("res/save/" + name + ".save");
		if (!f.exists()) 
			return null;
		ObjectInputStream ois;
		Task t = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(f));
			t = (Task) ois.readObject();
			ois.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return t;
	}
	
	public static ArrayList<Task> getTasks() {
		ArrayList<Task> tasks = new ArrayList<Task>();
		File[] files = (new File("res/save")).listFiles();
		for (File f : files) {
			tasks.add(new Task(f.getName().replaceAll(".save", "")));
		}
		return tasks;
	}
	
	private static int findId() {
		for (int k = 0; 0 <= k ; ++k) {
			if (!idList.contains(Integer.valueOf(k))) {
				return k;
			}
		}
		return -1;
	}
}
